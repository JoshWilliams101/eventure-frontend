# eventure-frontend

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

### Install [nodeJS](https://nodejs.org/en/)

### Clone repo
`git clone https://<your_bitbucket_username>@bitbucket.org/JoshWilliams101/eventure-frontend.git`

### Install Bower and global packages using npm

`npm install -g grunt-cli bower yo`
If you are planning on using Sass, you will need to first install Ruby and Compass:

Install Ruby by downloading from [here](http://rubyinstaller.org/downloads/) or use Homebrew
Install the compass gem:
`gem install compass`


### Install all the dependencies
`cd project_folder`
`bower install && npm install`


### Starting the server
`grunt serve --force`

* This will automatically open up in your default browser.

### Additional configuration
** Note do this after you start the server **
After running the server you need to do some additional setup. 
in **/app/index.html**

Make sure there is a **"/"** before bower_components to the file paths located under
`<!-- build:css(.) styles/vendor.css -->`

`<!-- bower:css -->`


so if a path starts like this for i.e: "bower_components/angular...." change it to "/bower_components/angular...."

* This is where things get tricky
Find the bootstrap.js files that is loaded under

` <!-- build:js(.) scripts/vendor.js -->`

`<!-- bower:js -->`

** Note that you SHOULD NOT get rid of the `<script src="bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js"></script>` **

There might be another bootstrap.js file that was automatically injected onto the page because of yeoman which messes up bootstrap. Just get rid of that line of code.


### After that you should be all set up
* go to `http://localhost:3000` and the app should load. The backend server needs to be running as well.


for any inquiries contact jwilliams199055@gmail.com