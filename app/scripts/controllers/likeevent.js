'use strict';

/**
 * @ngdoc function
 * @name Eventure.controller:LikeeventCtrl
 * @description
 * # LikeeventCtrl
 * Controller of the Eventure
 */
var app = angular.module('Eventure');

app.controller('LikeEventController', ['$routeParams', '$scope', '$rootScope', 'Restangular', function($routeParams, $scope, $rootScope, Restangular) {

  //if user clicks like
  $scope.like = function() {
    console.log("This is like");

    //if user is logged in
    if ($rootScope.currentUser) {
      //rating to be added
      $scope.rate = {
        rating: true
      };

      //base rate event for rating an event based on eventid
      var baseLeave = Restangular.one("rate/event/", $routeParams.eventId);
      //like the event based on eventid
      baseLeave.customPOST($scope.rate)
        .then(function(data) {
          console.log(data);
        })
        .catch(function(err) {
          console.log(err);
        });
    }
    else{

    }
  };

  $scope.unlike = function() {
    console.log("this is unlike");
    if ($rootScope.currentUser) {
      //rating to be added
      $scope.rate = {
        rating: false
      };

      //base rate event for rating an event based on eventid
      var baseLeave = Restangular.one("rate/event/", $routeParams.eventId);
      //like the event based on eventid
      baseLeave.customPOST($scope.rate)
        .then(function(data) {
          console.log(data);
        })
        .catch(function(err) {
          console.log(err);
        });
    }
    else{

    }
  };

}]);
