'use strict';

/**
 * @ngdoc function
 * @name Eventure.controller:NavbarcontrollerCtrl
 * @description
 * # NavbarcontrollerCtrl
 * Controller of the Eventure
 */
var app = angular.module('Eventure');


app.controller('NavbarController', ['$scope', '$uibModal','$document','Socket', '$rootScope','Restangular','$timeout', function ($scope, $uibModal, $document, SocketFactory, $rootScope, Restangular, $timeout) {

	$scope.notificationsOpened = false;

	$scope.openlogin = function () {
		var parentElem = angular.element($document[0].querySelector('#loginmodal-wrapper'));

		var modalInstance = $uibModal.open({
			templateUrl: '../../views/partials/login-modal.html',
			controller: 'LoginController',
			appendTo: parentElem
		});

		modalInstance.result.then(function () {}, function () {});
	}


	$scope.opensignup = function () {
		var parentElem = angular.element($document[0].querySelector('#signupmodal-wrapper'));

		var modalInstance = $uibModal.open({
			templateUrl: '../../views/partials/signup-modal.html',
			controller: 'SignupController',
			appendTo: parentElem
		});

		modalInstance.result.then(function () {}, function () {});
	}

	$scope.opennotifications = function(){
		var parentElem = angular.element($document[0].querySelector('#notificationwrapper'));

		var modalInstance = $uibModal.open({
			size: 'lg',
			templateUrl: 'notifcations-modal.html',
			controller: 'NotificationsController',
			appendTo: parentElem
		});

		modalInstance.opened.then(function () {
			$scope.notificationsOpened = true;
		});

		modalInstance.result.then(function() {
			$scope.notificationsOpened = false;
			$scope.getNotifications();

		}, function() {
			$scope.notificationsOpened = false;
			$scope.getNotifications();
		});
	};

	$scope.opencalendar = function () {
		var parentElem = angular.element($document[0].querySelector('#calendarwrapper'));

		var modalInstance = $uibModal.open({
			size: 'lg',
			templateUrl: 'calendar-modal.html',
			controller: 'CalendarController',
			appendTo: parentElem
		});

		modalInstance.result.then(function() {}, function() {});
	}





	//notifications for sockets
	//store main socket from Factory
  var socket = $rootScope.mainSocket;
	console.log(socket ," is the socket for navbar");
	var baseNotifications = Restangular.all('notifications');

	$scope.notificationCounter = 0;



	$scope.getNotifications = function () {
		baseNotifications.customGET("", {}).then(function (data) {
			$timeout(function () {
				$scope.notificationCounter = data.data.length || 0;
			});
		})
	};

	$rootScope.$watch('currentUser', function (user) {
		if(user) {
			$scope.getNotifications();
		}
	})



	$rootScope.$watch('currentUser', function(user) {
			if(user) {
				socket.emit('join', user.id);

				console.log("join room " + user.id);
			}
    },
  false);

	socket.on('notify', function (data) {
		$scope.getNotifications();
	});

}]);

//
app.controller('NotificationsController', ['$scope', '$uibModalStack', '$timeout', 'Restangular', 'Socket', '$http', '$location', '$rootScope', function ($scope, $uibModalStack, $timeout, Restangular, SocketFactory, $http, $location, $rootScope) {

	$scope.notifications = [];

	$scope.autoUpdate = 'ON';

	var socket = $rootScope.mainSocket;

	console.log(socket);

	$scope.print = function () {
		console.log(socket);
	};


	var baseNotifications = Restangular.all('notifications');

	var basePostNotifications = Restangular.one('notifications');

	socket.on('notify', function (data) {
		console.log("inside the notification controller notify");
		if($scope.autoUpdate === 'ON') {
			$scope.getNotifications();
		}
	});

	$scope.closemodal = function(){
		$uibModalStack.dismissAll();
	}

	$scope.viewall = function () {
		$location.path('notifications');
	}


	$scope.getNotifications = function () {


		baseNotifications.customGET("", {}).then(function (data) {

			$timeout(function () {
				if(!isArray(data.data) && data.data) {
					$scope.notifications.push(data.data);
				} else if(isArray(data.data) && data.data) {
					//then this is array
					$scope.notifications = data.data;
				} else {
					$scope.notifications = [];
				}
			});

		})
	};

	$scope.markread = function (id) {
		$http.post("http://localhost:9000/api/v1/notifications/mark_read/" + id, {}).then(function (data) {
			$scope.getNotifications();
		});
	}

	//get notifications
	$scope.getNotifications();
}]);


function isArray(what) {
    return Object.prototype.toString.call(what) === '[object Array]';
}
