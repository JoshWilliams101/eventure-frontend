'use strict';

/**
 * @ngdoc function
 * @name Eventure.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the Eventure
 */
angular.module('Eventure')
  .controller('ProfileController', ['$scope', '$rootScope', 'Restangular', '$timeout', '$http', 'Upload','$location', '$compile', 'Notification', function($scope, $rootScope, Restangular, $timeout, $http, Upload, $location, $compile, Notification) {




    //instantiating variable for user's joined events
    $scope.userJoinedEvents = [];
    $scope.user = {};
    $scope.userCreatedEvents = [];
    $scope.profile = {
      firstname: "",
      lastname: "",
      date_of_birth: "",
      categories: [],
      gender: "",
      profile_pic_url: "",
      file: null,
    };

    //if user is signed in
    if ($rootScope.currentUser) {
      //base user for getting user by id
      var baseUser = Restangular.all('users/' + $rootScope.currentUser.id);

      baseUser.customGET("", {}).then(function (user) {

        angular.forEach(user.data.categories, function ($obj, $index) {
          console.log($scope.profile);
          $scope.profile.categories.push($obj.id);
        });

        console.log("user categories are: ", $scope.profile.categories);

        //set baseCategories to base categories api route
        var baseCategories = Restangular.all('categories');
        //get all categories
        baseCategories.customGET("", {}).then(function(categories) {
          $scope.categories = categories.data;
        });
      });
    }

    $scope.today = new Date();



    //if user is signed in
    if ($rootScope.currentUser) {
      //base user events for getting joined events based on userid
      var baseUserEvents = Restangular.all('users/' + $rootScope.currentUser.id + '/joinedEvents');

      //get joined events based on userid
      baseUserEvents.customGET("", {}).then(function(events) {

        //loop through each event to get the categories with them
        angular.forEach(events.data, function($obj, $index) {

          $scope.userJoinedEvents.push($obj);


          $http.get("http://localhost:9000/api/v1/event/categories/" + $obj.id, {})
            .success(function(categories) {


              if (categories.data.length > 0) {
                $obj.categories = categories.data;
              }
            })
            .error(function(err) {
              Notification.error({message: err.message, positionX: 'left'});
            })
        })
      });
    }

    //if user is signed in
    if ($rootScope.currentUser) {
      //base user events for getting created events based on userid
      var baseUserCreatedEvents = Restangular.all('users/' + $rootScope.currentUser.id + '/createdEvents');

      //get created events based on userid
      baseUserCreatedEvents.customGET("", {}).then(function(event) {
        $scope.userCreatedEvents = event.data;
      });
    }









    //Profile Settings
    //create profile obj
    //default
    $timeout(function() {
      $scope.txtfirstname = true;
      $scope.txtlastname = true;
      $scope.rbgender = true;
      $scope.checkcategory = true;
    });

    $scope.userFirstName = null;
    $scope.userLasttName = null;

    $timeout(function () {
      if($rootScope.currentUser.first_name != null){
        $scope.firstname = $rootScope.currentUser.first_name;
        $scope.userFirstName =  $rootScope.currentUser.first_name;
      }
      if($rootScope.currentUser.last_name != null){
        $scope.lastname = $rootScope.currentUser.last_name;
        $scope.userLasttName = $rootScope.currentUser.last_name;
      }
      if($rootScope.currentUser.date_of_birth != null){
        $scope.birthdate = $rootScope.currentUser.date_of_birth;
      }
      if($rootScope.currentUser.gender == "female"){
      }
      if($rootScope.currentUser.gender == "male"){
            $scope.male = true;
      }
    });


    //enable edit information
    $scope.editfirstname = function(){
      $timeout(function() {
        $scope.editfirst = true;
        $scope.txtfirstname = false;
      });
    }
    $scope.editlastname = function(){
      $timeout(function() {
        $scope.editlast = true;
        $scope.txtlastname = false;
      });
    }


    //disable edit information
    $scope.disablefirstname = function(){
      $timeout(function() {
        $scope.profile.firstname = angular.copy($scope.userFirstName);
        $scope.editfirst = false;
        $scope.txtfirstname = true;
      });
    }

    $scope.disablelastname = function(){
      $timeout(function() {
        $scope.profile.lastname = angular.copy($scope.userLasttName);
        $scope.editlast = false;
        $scope.txtlastname = true;
      });
    }

    //on picture selected change profile file obj
    $scope.setFile = function(file, thing) {
  		//set file to scope variable
      if ($scope.profile.file) {
        Upload.upload({
            url: 'http://localhost:9000/api/v1/uploads/profile/picture',
            data: {
              file: $scope.profile.file,
            }
          }).then(function(resp) {
      $timeout(function () {
  		    $scope.currentUser.profile_pic_url = resp.data.data;
      });
    });
    }
    }

    //uib datepicker code
    $scope.datepickerOptions = {
      showWeeks: true,
      initDate: new Date(),
    }

    $scope.start_date_picker = {
      opened: false
    };

    $scope.open_sd = function() {
      $scope.start_date_picker.opened = true;
    };

    $scope.altFormat = ['M!/d!/yyyy'];

    $scope.format = 'MMMM d, y';


      //START OF UPDATE FUNCTION
      $scope.update = function(profile) {
          //put request to user
          $http.post("http://localhost:9000/api/v1/users/" +  $rootScope.currentUser.id + "/categories", {categories: profile.categories})
            .success(function(data) {
              console.log(data);
              //socket.emit('createdEvent', data.data.id);
              $location.path('main');
            })
            .error(function(err, status) {
              $scope.submitting = false;
              Notification.error({message: err.message, positionX: 'left'});

            });
          //if a file is selected
          if (profile.file) {
            Upload.upload({
                url: 'http://localhost:9000/api/v1/uploads/profile/picture',
                data: {
                  file: profile.file,
                }
              })
              //on success
              .then(function(resp) {
                  //submit everything else if true
                  if (resp.data.success) {
                    //create the obj that will be sent through to the api
                    $scope.profile = {
                      first_name: profile.firstname,
                      last_name: profile.lastname,
                      date_of_birth: profile.date_of_birth,
                      gender: profile.gender,
                      profile_pic_url: resp.data.data,
                    };

                    //put request to user
                    $http.put("http://localhost:9000/api/v1/users/" +  $rootScope.currentUser.id, $scope.profile)
                      .success(function(data) {
                        //socket.emit('createdEvent', data.data.id);
                        $location.path('profile');
                      })
                      .error(function(err, status) {
                        $scope.submitting = false;
                        Notification.error({message: err.message, positionX: 'left'});

                      });
                  }


                },
                //on error
                function(resp) {
                  $scope.submitting = false;
                  Notification.error({message: resp.status + ' error uploading file', positionX: 'left'});
                },
                //event
                function(evt) {
                  var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                  console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
          }
          //if image has not been uploaded
          else {
            //create the obj that will be sent through to the api
            $scope.profile = {
              first_name: profile.firstname,
              last_name: profile.lastname,
              date_of_birth: profile.date_of_birth,
              gender: profile.gender,
              profile_pic_url: null,
            };

            console.log("profile is: ", $scope.profile);


            //update event
            $http.put("http://localhost:9000/api/v1/users/" +  $rootScope.currentUser.id, $scope.profile)
              .success(function(data) {
                Notification({message: "Profile Updated!", positionX: 'left'});
                $location.path('main');
              })
              .error(function(err, status) {
                $scope.submitting = false;
                Notification.error({message: err.message, positionX: 'left'});
              });
          };

        }
        //END OF UPDATE FUNCTION

  }]);
