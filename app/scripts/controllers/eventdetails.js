'use strict';

/**
 * @ngdoc function
 * @name Eventure.controller:EventdetailsCtrl
 * @description
 * # EventdetailsCtrl
 * Controller of the Eventure
 */
var app = angular.module('Eventure');

app.controller('EventDetailsController', ['$scope', 'Restangular', '$routeParams', '$rootScope', '$timeout', '$uibModal', '$document', 'Upload', '$location', '$http', '$route', 'Notification', function($scope, Restangular, $routeParams, $rootScope, $timeout, $uibModal, $document, Upload, $location, $http, $route, Notification) {

  $scope.now = Date.now();

  //set baseCategories to base categories api route
  var baseCategories = Restangular.all('categories');
  //get all categories
  baseCategories.customGET("", {}).then(function(categories) {
    $scope.categories = categories.data;

    //set rules for categories

    //set checked limit
    $scope.categoryLimit = 3;
  });

  //base categories for specified event by ID
  var baseEventCategories = Restangular.one('/event/categories/', $routeParams.eventId);
  //get categories by event id
  baseEventCategories.customGET("", {}).then(function(event) {
    $scope.eventcategories = event.data;
    //  console.log($scope.eventcategories[0].name);
    for (var i = 0; i < $scope.eventcategories.length; i++) {
      var eventcat = $scope.eventcategories[i].name;
      $timeout(function() {
        $scope.eventcat = true;
      });
    }
  });

  //store main socket from Factory
  var socket = $rootScope.mainSocket;


  socket.on("updateAttendees", function() {
    $scope.updateAttendees();
  });

  socket.on("updateLikesDislikes", function() {
    $scope.getLikesDislikes();
  });
  $scope.editBtnText = "EDIT";

  //create newEvent obj
  $scope.newEvent = {
    categories: [], //set selected to empty json obj 'which will contain keys of the categories selected'
    file: null,
  };
  $scope.user = null;
  var online = false;
  $scope.password = false;
  $scope.editmode = false;
  $scope.online = false;
  //default $scope values
  $scope.event = {};

  $scope.isOwner = false;
  $scope.ownersEvents = [];
  $scope.joined = false;
  $scope.edit = true;

  $scope.likes = 0;
  $scope.dislikes = 0;
  $scope.attendees = 0;
  $scope.commentsNo = 0;

  //map variables
  $scope.directionShown = false;
  $scope.origin = "";

  //base attendees to grab atendees count
  var baseAttendees = Restangular.all("event/users/" + $routeParams.eventId + "/count");
  $scope.updateAttendees = function() {
    baseAttendees.customGET().then(function(count) {
      //update attendees
      $timeout(function() {
        $scope.attendees = count.data;
      });
    });
  };


  //code to like or dislike event
  var baseRating = Restangular.one("rate/event/", $routeParams.eventId);

  var baseLikes = Restangular.all("ratings/event/" + $routeParams.eventId + "/likes");
  var baseDislikes = Restangular.all("ratings/event/" + $routeParams.eventId + "/dislikes");


  $scope.getLikesDislikes = function() {
    //do request to grab likes
    baseLikes.customGET().then(function(count) {
      console.log(count);
      $timeout(function() {
        $scope.likes = count.data;
      });
    });
    //do a request to grab dislikes
    baseDislikes.customGET().then(function(count) {
      console.log(count);
      $timeout(function() {
        $scope.dislikes = count.data;
      });
    });
  }

  //enable edit if edit clicked
  $scope.editEvent = function() {
    $timeout(function() {
      $scope.edit = false;
      $scope.eventaddress = $scope.event.address;
      $scope.eventname = $scope.event.name;
      $scope.eventstart = $scope.event.start_date;
      $scope.eventend = $scope.event.end_date;
      tinyMCE.activeEditor.setContent($scope.event.html_description);
    });
  }

  //Geocode the address from input
  $scope.placeChanged = function() {
    var place = this.getPlace();
    $scope.newEvent.address = place.formatted_address;
    console.log(place);
    $timeout(function() {
      $scope.event.latitude = place.geometry.location.lat() || 43.6574088;
      $scope.event.longitude = place.geometry.location.lng() || -79.7437504;
    });
  }

  //cancel edit event
  $scope.cancel = function() {
    $timeout(function() {
      $scope.edit = true;
    });
  }

  //uib datepicker code
	$scope.datepickerOptions = {
		minDate: new Date(),
		showWeeks: true,
		initDate: new Date(),
	}

	$scope.edit = function(){
		$timeout(function () {
			$scope.edit = false;
		});
	};


  //set calendar as false
  $scope.start_date_picker = {
    opened: false
  };

  $scope.end_date_picker = {
    opened: false
  };

  //open start date calendar
  $scope.open_sd = function() {
    $scope.start_date_picker.opened = true;
  };

  //open end date calendar
  $scope.open_ed = function() {
    $scope.end_date_picker.opened = true;
  };

  $scope.altFormat = ['M!/d!/yyyy'];

  $scope.format = 'EEEE, MMMM d, y h:mm a';

  //timepicker code starts here

  $scope.hstep = 1;
  $scope.mstep = 5;
  $scope.ismeridian = true;


  $scope.onTimepickerChanged = function(date) {
    console.log(date);
  };
  //timepicker  code ends here, uib datepicker ends here


  //START OF DELETE EVENT
  $scope.deleteEvent = function() {
    //base categories for specified event by ID
    var baseEventDelete = Restangular.one('/events/' + $routeParams.eventId);
    //get categories by event id
    baseEventDelete.customDELETE("", {}).then(function() {
      //loop through each user and send them a notification
      for(var i = 0; i < $scope.event.users.length; i++) {

        //send notification to all users attending this event that it has been deleted
        socket.emit('event deleted', {
          receiverId: $scope.event.users[i].user_id,
          senderId:  $rootScope.currentUser.id,
          event_id: $scope.event.id,
          event_name: $scope.event.name
        });

      }
      //go back to main page
      $location.path('main');

    }).catch(function (err) {
      console.log(err);
    });
  }



  //START OF UPDATE FUNCTION
  $scope.update = function(newEvent) {
      newEvent.private = ($scope.newEvent.private === "private");
      //if a file is selected
      if (newEvent.file) {
        Upload.upload({
            url: 'http://localhost:9000/api/v1/uploads/event/picture',
            data: {
              file: newEvent.file,
            }
          })
          //on success
          .then(function(resp) {
              //submit everything else if true
              console.log(resp);
              if (resp.data.success) {
                Notification.success({message: "File Upload Successful", positionX: 'left'});

                //create the obj that will be sent through to the api
                $scope.editedEvent = {
                  name: newEvent.name,
                  description: newEvent.description,
                  categories: newEvent.categories,
                  start_date: newEvent.start_date,
                  end_date: newEvent.end_date,
                  private: newEvent.private,
                  address: (online) ? "This is an online event" : newEvent.address,
                  //categories: newEvent.categories,
                  picture_url: resp.data.data,
                  latitude: (online) ? 0.0 : $scope.event.latitude,
                  longitude: (online) ? 0.0 : $scope.event.longitude,
                  password: newEvent.password
                };


                $http.put("http://localhost:9000/api/v1/events/" + $routeParams.eventId, $scope.editedEvent)
                  .success(function(data) {

                    Notification({message: "Event Successfully Updated", positionX: 'left'});

                    //loop through each user and send them a notification
                    for(var i = 0; i < $scope.event.users.length; i++) {
                      //send notification to all users attending this event that it has been updated
                      socket.emit('event edited', {
                        receiverId: $scope.event.users[i].user_id,
                        senderId:  $rootScope.currentUser.id,
                        event_id: $scope.event.id,
                        event_name: $scope.event.name
                      });

                    }
                    $route.reload();
                  })
                  .error(function(data, status) {
                    $scope.submitting = false;
                    console.error(data);
                  });
              }


            },
            //on error
            function(resp) {
              $scope.submitting = false;
              console.log('Error status: ' + resp.status);
            },
            //event
            function(evt) {
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
      }
      //if image has not been uploaded
      else {


        //create the obj that will be sent through to the api
        $scope.editedEvent = {
          name: newEvent.name,
          description: newEvent.description,
          start_date: newEvent.start_date,
          end_date: newEvent.end_date,
          private: newEvent.private,
          categories: newEvent.categories,
          address: (online) ? "This is an online event" : newEvent.address,
          //categories: newEvent.categories,
          picture_url: null,
          latitude: (online) ? 0.0 : $scope.event.latitude,
          longitude: (online) ? 0.0 : $scope.event.longitude,
          password: newEvent.password
        };

        //update event
        $http.put("http://localhost:9000/api/v1/events/" + $routeParams.eventId, $scope.editedEvent)
          .success(function(data) {


            Notification({message: "Event Successfully Updated", positionX: 'left'});

            //loop through each user and send them a notification
            for(var i = 0; i < $scope.event.users.length; i++) {


              //send notification to all users attending this event that it has been updated
              socket.emit('event edited', {
                receiverId: $scope.event.users[i].user_id,
                senderId:  $rootScope.currentUser.id,
                event_id: $scope.event.id,
                event_name: $scope.event.name
              });

            }
            $route.reload();

          })
          .error(function(data, status) {
            $scope.submitting = false;
            console.error(data);
          });
      };

    }
    //END OF UPDATE FUNCTION

  $scope.likeEvent = function() {
    baseRating.customPOST({
      rating: true
    }).then(function(data) {
      if (data.success) {
        Notification.success({message: "You liked this event", positionX: 'left'});

        socket.emit("edUpdateLikesDislikesRequest", "");
        socket.emit("event liked", {
          receiverId: $scope.event.owner.id,
          senderId: $rootScope.currentUser.id,
          name: $rootScope.currentUser.displayName,
          event_id: $scope.event.id,
          event_name: $scope.event.name,
        });
        $scope.getLikesDislikes();
      }
    });
  }

  $scope.dislikeEvent = function() {
    baseRating.customPOST({
      rating: false
    }).then(function(data) {
      if (data.success) {
        socket.emit("edUpdateLikesDislikesRequest", "");
        $scope.getLikesDislikes();
      }
    });
  }


  //code for google maps show directions
  $scope.showDirections = function() {
    $timeout(function() {
      $scope.directionShown = true;
    });
  };

  $scope.hideDirections = function() {
    $timeout(function() {
      $scope.origin = "";
      $scope.directionShown = false;
    });
  }


  //base Events for specified event by ID
  var baseEvents = Restangular.one('events', $routeParams.eventId);

  //base Users route
  var baseUser = Restangular.all('users');

  //get event based on ID
  baseEvents.customGET("", {}).then(function(event) {

      //if event.data is null set $scope.event to null and return
      if(!event.data) {
        $scope.event = null;
        return;
      }


    //save event scope variable
    $scope.event = event.data;

    angular.forEach($scope.event.categories, function ($obj, $index) {
      $scope.newEvent.categories.push($obj.id);
    });

    console.log("event categories are: ", $scope.newEvent.categories);


    //conver start and end date into actual dates
    $scope.event.start_date = new Date($scope.event.start_date);
    $scope.event.end_date = new Date($scope.event.end_date);


    //set owners display name
    $scope.event.owner.displayName = $scope.event.owner.username || $scope.event.owner.facebook_name || $scope.event.owner.twitter_displayName || $scope.event.owner.google_name;

    //check if user joined this event
    $scope.checkIfJoined();
    //check if event is private
    $scope.checkIfPrivate();
    //check if event is online
    $scope.checkIfOnline();

    //set number of users attending
    $scope.attendees = $scope.event.users.length || 0;

    //check to see if the user is the owner of this event
    $timeout(function() {
      $scope.isOwner = ($rootScope.currentUser && $rootScope.currentUser.id === $scope.event.user_id);
    });

    if ($scope.event.latitude && $scope.event.longitude) {
      $timeout(function() {
        $scope.notonline = true;
      });
    } else {
      $timeout(function() {
        $scope.notonline = false;
      });
    }

    //search all of the event owners created events based on $scope.event.user_id
    /*	baseUser.customGET($scope.event.user_id + "/createdEvents?eventNotEqual=" + $routeParams.eventId, {}).then(function (events) {
    		$scope.ownersEvents = events.data;
    	}); */

    //base user for specified user by ID
    var baseUser = Restangular.one('/users/' + $scope.event.user_id);
    //get user by user id
    baseUser.customGET("", {}).then(function(event) {
      $scope.user = event.data;
      console.log($scope.user);
    });

  });



  //Check if online
  $scope.checkIfOnline = function() {
    if ($scope.event.address == "This is an online event") {
      $scope.online = true;
      $scope.event.latitude = false;
      $scope.event.longitude = false;
      online = true;
    }
  }

  //Check to see if the event is private or not
  $scope.checkIfPrivate = function() {
    if ($scope.event.private) {
      console.log("This is a private event");
      $timeout(function() {
        $scope.private = true;
        $scope.password = true;
        $scope.passwordplaceholder = "Change Password";
      });
    } else {
      console.log("This is public event");
      $timeout(function() {
        $scope.passwordplaceholder = "Enter Password";
        $scope.public = true;
      });
    }
  };

  $scope.showMap = function() {
    $timeout(function() {
      $scope.event.latitude = 43.6560583;
      $scope.event.longitude = -79.7415324;
      $scope.online = false;
      online = false;
    });
  };

  $scope.hideMap = function() {
    $timeout(function() {
      $scope.event.latitude = false;
      $scope.event.longitude = false;
      $scope.online = true;
      online = true;
    });
  };

  // NG - MAP code starts here
  $scope.map = {
    center: {
      latitude: 43.6574088,
      longitude: -79.7437504
    },
  };

  //if private event
  $scope.privateClicked = function() {
    $timeout(function() {
      //enable password input
      $scope.password = true;
    });
  };

  //if public event
  $scope.publicClicked = function() {
    $timeout(function() {
      //disable password input
      console.log($scope.newEvent.private);
      $scope.password = false;
    });
  };


  var baseRatings = Restangular.all('ratings');

  $scope.getLikesDislikes();


  //watch $rootScope.currentUser to check if they log in, if they do check to see if they joined the event
  $rootScope.$watch('currentUser', $scope.checkIfJoined);


  //function to check if user has joined event
  $scope.checkIfJoined = function() {

    //set joined to false
    $scope.joined = false;
    //if event has attendees
    if ($scope.event.users && $rootScope.currentUser) {
      //check to see if user has joined this event
      for (var i in $scope.event.users) {
        //if the user is logged in and has joined this event set joined value to true
        if ($rootScope.currentUser.id === $scope.event.users[i].user_id) {
          $scope.joined = true;
          break;
        }
      }
    }
  }



  //function to join an event
  $scope.join = function() {

    if (!$scope.event.password) {

      var baseJoin = Restangular.one("events/join", $routeParams.eventId);

      //let user join event
      baseJoin.customPOST()
        .then(function(data) {
          if (data.success) {
            Notification.success({message: "Event joined", positionX: 'left'});

            socket.emit("edUpdateAttendeesRequest", "");
            socket.emit("event joined", {
              receiverId: $scope.event.owner.id,
              senderId: $rootScope.currentUser.id,
              name: $rootScope.currentUser.displayName,
              event_id: $scope.event.id,
              event_name: $scope.event.name,
            });
            $scope.joined = true;
            $scope.updateAttendees();
          }
        });
    }
    //else open password request modal
    else {

      var parentElem = angular.element($document[0].querySelector('#password-request-modal'));

      var modalInstance = $uibModal.open({
        templateUrl: 'passwordModal.html',
        controller: 'PasswordRequestController',
        controllerAs: '$ctrl',
        size: 'sm',
        resolve: {
          eventId: $scope.event.id
        },
        appendTo: parentElem
      });

      modalInstance.result
        .then(function(result) {

          //if the returned modal value is true, that means they typed in the right password
          if (result) {
            socket.emit("edUpdateAttendeesRequest", "");
            $scope.joined = true;
            $scope.updateAttendees();

          }

        }, function() {});

    }
  };
  //function to leave an event
  $scope.leave = function() {
    var baseLeave = Restangular.one("events/leave", $routeParams.eventId);
    baseLeave.customPOST()
      .then(function(data) {
        if (data.success) {
          Notification.error({message: "Event left", positionX: 'left'});

          socket.emit("edUpdateAttendeesRequest", "");
          $scope.joined = false;
          $scope.updateAttendees();
        }
      })
      .catch(function(err) {

      });
  };



  //for pagination
  $scope.currentPage = 1;
  $scope.pageSize = 3;

  //disqus config, passes stuff to dirDisqus directive to diffirientiate disqus threads
  //needa do SSO
  $scope.disqusConfig = {
    disqus_shortname: 'eventure',
    disqus_identifier: '' + $routeParams.eventId,
    disqus_url: 'http://localhost:3000/eventDetails/' + $routeParams.eventId
  };


	//disqus config, passes stuff to dirDisqus directive to diffirientiate disqus threads
	//needa do SSO
	$scope.disqusConfig = {
	    disqus_shortname: 'eventure',
	    disqus_identifier: '' + $routeParams.eventId,
			disqus_url: 'http://localhost:3000/eventDetails/' + $routeParams.eventId
	};


 }]);


//controller for password modal
app.controller("PasswordRequestController", ['$scope', '$uibModalInstance', 'Restangular', 'eventId', function($scope, modalInstance, Restangular, eventId) {

  $scope.accepted = false;
  $scope.passwordError = "";

  $scope.close = function() {
    console.log("wow can you close?");

    modalInstance.close($scope.accepted);
  };


  console.log(eventId);
  var baseJoin = Restangular.one("events/join", eventId);

  // join with password
  $scope.join = function() {


    //let user join event
    baseJoin.customPOST({
        password: $scope.password
      })
      .then(function(data) {

        console.log(data);
        if (data.success) {
          $scope.accepted = true;
          modalInstance.close($scope.accepted);
        } else {
          $scope.passwordError = data.message;
        }
      });


  }

}]);
