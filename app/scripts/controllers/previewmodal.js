'use strict';

/**
 * @ngdoc function
 * @name Eventure.controller:PreviewmodalCtrl
 * @description
 * # PreviewmodalCtrl
 * Controller of the Eventure
 */
angular.module('Eventure')
  .controller('PreviewModalController', ['$scope', '$uibModalInstance', 'event', function($scope, $uibModalInstance, event) {
    $scope.event = event;
    $scope.closemodal = function(){
      $uibModalInstance.close();
    }

    $scope.mapInit = function (map) {


      map.trigger('resize');
    }

  }]);
