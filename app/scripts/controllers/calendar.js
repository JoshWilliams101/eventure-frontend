'use strict';

/**
 * @ngdoc function
 * @name Eventure.controller:CalendarCtrl
 * @description
 * # CalendarCtrl
 * Controller of the Eventure
 */
var app = angular.module('Eventure');
app.controller('CalendarController', ['$scope', 'Restangular','$timeout', '$uibModalStack', '$compile', '$rootScope', '$location', function ($scope, Restangular, $timeout, $uibModalStack, $compile, $rootScope, $location) {

  var socket = $rootScope.mainSocket;

    $scope.closemodal = function () {
      $uibModalStack.dismissAll();
    };

    $scope.calendarView = 'month';

    $scope.viewDate = new Date();

    $scope.events = [];

    $scope.calendarTitle = "My Calendar";


    $scope.eventClicked = function (calendarEvent) {
      $location.path('eventDetails/' + calendarEvent.id);
    }


    $scope.getCalendarEvents = function () {

      $scope.events = [];

      //add the events to the sources through api calls
      //base user events for getting joined events based on userid
      var baseUserEvents = Restangular.all('users/' + $rootScope.currentUser.id + '/joinedEvents');

      //get joined events based on userid
      baseUserEvents.customGET("", {}).then(function(events) {

        //loop through each event to get the categories with them
        angular.forEach(events.data, function($obj, $index) {

          $scope.events.push({
            id: $obj.id,
            title: $obj.name, // The title of the event
            startsAt: new Date($obj.start_date), // A javascript date object for when the event starts
            endsAt: new Date($obj.end_date), // Optional - a javascript date object for when the event ends
            color: { // can also be calendarConfig.colorTypes.warning for shortcuts to the deprecated event types
              primary: '#32cd32', // the primary event color (should be darker than secondary)
              secondary: 'white' // the secondary event color (should be lighter than primary)
            },
            actions: [{ // an array of actions that will be displayed next to the event title
              label: '<i class=\'fa fa-times-circle text-danger\'></i>', // the label of the action
              onClick: function(args) { // the action that occurs when it is clicked. The first argument will be an object containing the parent event
                console.log('leave event', args.calendarEvent);
                var baseLeave = Restangular.one("events/leave", args.calendarEvent.id);
                baseLeave.customPOST()
                  .then(function(data) {
                    if (data.success) {
                      socket.emit("edUpdateAttendeesRequest", "");
                      $scope.getCalendarEvents();
                    }
                  })
                  .catch(function(err) {
                    console.error(err);
                  });
              }
            }],
            resizable: true, //Allow an event to be resizable
            allDay: true // set to true to display the event as an all day event on the day view
          });
        });
      });

      //
      //base user events for getting created events based on userid
      var baseUserCreatedEvents = Restangular.all('users/' + $rootScope.currentUser.id + '/createdEvents');

      //get created events based on userid
      baseUserCreatedEvents.customGET("", {}).then(function(events) {
        //loop through each event to get the categories with them
        angular.forEach(events.data, function($obj, $index) {

          $scope.events.push({
            id: $obj.id,
            title: $obj.name, // The title of the event
            startsAt: new Date($obj.start_date), // A javascript date object for when the event starts
            endsAt: new Date($obj.end_date), // Optional - a javascript date object for when the event ends
            color: { // can also be calendarConfig.colorTypes.warning for shortcuts to the deprecated event types
              primary: '#1E90FF', // the primary event color (should be darker than secondary)
              secondary: 'white' // the secondary event color (should be lighter than primary)
            },
            resizable: true, //Allow an event to be resizable
            allDay: true // set to true to display the event as an all day event on the day view
          });

        });
      });
    };

    $scope.getCalendarEvents();





  }]);
