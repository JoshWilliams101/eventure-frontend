'use strict';

/**
 * @ngdoc function
 * @name eventureFrontendApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the eventureFrontendApp
 */
var app = angular.module("Eventure");
app.controller('LoginController',['$scope','Authentication','$uibModalInstance','$timeout','$document','$uibModal', function ($scope, Authentication, $uibModalInstance, $timeout, $document, $uibModal) {

  $scope.login = function () {
    Authentication.login($scope.user, $uibModalInstance);
  }; //login

  $scope.toSignUp = function () {

    console.log("hello");

    //close current modal
    $uibModalInstance.close();

    $timeout(function () {

    	var parentElem = angular.element($document[0].querySelector('#signupmodal-wrapper'));


    	var modalInstance = $uibModal.open({
    		templateUrl: '../../views/partials/signup-modal.html',
    		controller: 'SignupController',
    		appendTo: parentElem
    	});

    	modalInstance.result.then(function () {}, function () {});
    });
  }
}]);
