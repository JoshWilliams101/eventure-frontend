'use strict';

/**
 * @ngdoc function
 * @name Eventure.controller:EditeventCtrl
 * @description
 * # EditeventCtrl
 * Controller of the Eventure
 */
angular.module('Eventure')
  .controller('EditEventController', ['$scope', '$http', 'Restangular', 'Socket', '$timeout', 'Upload', '$location', '$routeParams', function($scope, $http, Restangular, SocketFactory, $timeout, Upload, $location, $routeParams) {
    //default $scope values
    $scope.event = {};

    //is this an online event
    var online = false;
    //default
    $scope.enterAddress = false;
    //default
    $scope.online = false;

    //base Events for specified event by ID
    var baseEvents = Restangular.one('events', $routeParams.eventId);

    //get event based on ID
    baseEvents.customGET("", {}).then(function(event) {
      console.log(event.data);
      //save event scope variable
      $scope.event = event.data;

    });

    //if($scope.event.private = 0){
      console.log($scope.event.private);
    //}

    if($scope.event.address == "This is an online event"){
      console.log("online");
    }
    $timeout(function() {
    $scope.eventname = $scope.event.name;
    });

    //console.log($scope.event.address);
    if ($scope.event.address == "This is an online event"){
      $timeout(function() {
      online = true;
      $scope.enterAddress = false;
      $scope.online = true;
      });
    }else{
      $timeout(function() {
      $scope.addresss = $scope.event.address;
      online = false;
      $scope.online = false;
      $scope.enterAddress = true;
      });
    }
    //console.log($scope.newEvent.name);
    //$scope.newEvent.name = $scope.event.name;

    //code for tinymce html text editor
    $scope.tinyMceOptions = {
      height: '350px',
      plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code'
      ],
      toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      content_css: '//www.tinymce.com/css/codepen.min.css',
      setup: function(editor) {
        //onload
        $timeout(function() {
          $scope.tinyMceLoaded = true;
        });
      }
    };

    //uib datepicker code
    $scope.datepickerOptions = {
      minDate: new Date(),
      showWeeks: true,
      initDate: new Date(),
    }

    $scope.start_date_picker = {
      opened: false
    };

    $scope.end_date_picker = {
      opened: false
    };

    $scope.open_sd = function() {
      $scope.start_date_picker.opened = true;
    };

    $scope.open_ed = function() {
      $scope.end_date_picker.opened = true;
    };

    $scope.altFormat = ['M!/d!/yyyy'];

    $scope.format = 'EEEE, MMMM d, y h:mm a';

    //today
    $scope.sd_placeholder = new Date();

    //tommorow
    $scope.ed_placeholder = new Date();
    $scope.ed_placeholder = $scope.ed_placeholder.setDate($scope.sd_placeholder.getDate() + 1);


    //timepicker code starts here

    $scope.hstep = 1;
    $scope.mstep = 5;
    $scope.ismeridian = true;


    $scope.onTimepickerChanged = function(date) {
      console.log(date);
    };
    //timepicker  code ends here, uib datepicker ends here



    // NG - MAP code starts here
    $scope.map = {
      center: {
        latitude: $scope.event.latitude,
        longitude: $scope.event.longitude
      },
    };

    $scope.marker = {
      position: {
        latitude: $scope.event.latitude,
        longitude: $scope.event.longitude
      }
    }


    //Geocode the address from input
    $scope.geoCode = function() {
      geoCodeAddress($scope.newEvent.address.formatted_address, function(obj) {
        $timeout(function() {
          $scope.map.center.latitude = obj.lat || 43.6574088;
          $scope.marker.position.latitude = obj.lat || 43.6574088;
          $scope.map.center.longitude = obj.lng || -79.7437504;
          $scope.marker.position.longitude = obj.lng || -79.7437504;
        });
      });
    }

    //if private event
    $scope.private = function() {
      $timeout(function() {
        //enable password input
        $scope.password = true;
      });
    }

    //if public event
    $scope.public = function() {
      $timeout(function() {
        //disable password input
        $scope.password = false;
      });
    }

    $scope.showMap = function() {
      $timeout(function() {
        online = false;
        $scope.online = false;
        $scope.enterAddress = true;
      });
    }

    $scope.hideMap = function() {
      $timeout(function() {
        online = true;
        $scope.enterAddress = false;
        $scope.online = true;
      });
    }

    // NG - MAP code ends here

    //set baseCategories to base categories api route
    var baseCategories = Restangular.all('categories');

    //store main socket from Factory
    var socket = SocketFactory.mainSocket().socket;


    //create newEvent obj
    $scope.newEvent = {
      categories: [], //set selected to empty json obj 'which will contain keys of the categories selected'
      file: null,
    };

    //on picture selected change newEvent file obj
    $scope.setFile = function(file, thing) {
      //set file to scope variable
      $scope.newEvent.file = file;
    }

    //get all categories
    baseCategories.customGET("", {}).then(function(categories) {
      $scope.categories = categories.data;

      //set rules for categories

      //set checked limit
      $scope.categoryLimit = 3;
    });

    //form errors
    $scope.formErrors = {}

    //START OF SUBMIT FUNCTION
    /*  $scope.submit = function(newEvent) {

        newEvent.private = ($scope.newEvent.private === "private");

        //if a file is selected
        if (newEvent.file) {
          Upload.upload({
              url: 'http://localhost:9000/api/v1/uploads/event/picture',
              data: {
                file: newEvent.file,
              }
            })
            //on success
            .then(function(resp) {
                //submit everything else if true
                console.log(resp);
                if (resp.data.success) {
                  //create the obj that will be sent through to the api
                  $scope.event = {
                    name: newEvent.name,
                    description: newEvent.description,
                    start_date: newEvent.start_date,
                    end_date: newEvent.end_date,
                    private: newEvent.private,
                    address: (online) ? "This is an online event" : newEvent.address.formatted_address,
                    categories: newEvent.categories,
                    picture_url: resp.data.data,
                    latitude: (online) ? 0.0 : $scope.marker.position.latitude,
                    longitude: (online) ? 0.0 : $scope.marker.position.longitude,
                    password: newEvent.password
                  };

                  console.log("event is \n\n\n");
                  console.log($scope.event);

                  //post request to create event
                  $http.post("http://localhost:9000/api/v1/events", $scope.event)
                    .success(function(data) {
                      $location.path('main');
                      console.log(data);
                      socket.emit('createdEvent', data.data.id);
                    })
                    .error(function(data, status) {
                      alert(data);
                    });
                }


              },
              //on error
              function(resp) {
                console.log('Error status: ' + resp.status);
              },
              //event
              function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
              });
        }
        //if image has not been uploaded
        else {


          //create the obj that will be sent through to the api
          $scope.event = {
            name: newEvent.name,
            description: newEvent.description,
            start_date: newEvent.start_date,
            end_date: newEvent.end_date,
            private: newEvent.private,
            address: (online) ? "This is an online event" : newEvent.address.formatted_address,
            categories: newEvent.categories,
            picture_url: null,
            latitude: (online) ? 0.0 : $scope.marker.position.latitude,
            longitude: (online) ? 0.0 : $scope.marker.position.longitude,
          };

          //post request to create event
          $http.post("http://localhost:9000/api/v1/events", $scope.event)
            .success(function(data) {
              $location.path('main');
              console.log(data);
              socket.emit('createdEvent', data.data.id);
            })
            .error(function(data, status) {
              alert(data);
            });
        };

      }*/

    //END OF SUBMIT FUNCTION
  }]);

//This function takes the address and converts it into coordinates
function geoCodeAddress(addr, callback) {
  var geocoder = new google.maps.Geocoder();
  return geocoder.geocode({
    "address": addr
  }, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      var lat = results[0].geometry.location.lat();
      var lng = results[0].geometry.location.lng();
      var latlng = {
        lat: lat,
        lng: lng
      };
      callback(latlng);
    } else {
      //alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}
