'use strict';

/**
 * @ngdoc function
 * @name eventureFrontendApp.controller:SignupCtrl
 * @description
 * # SignupCtrl
 * Controller of the eventureFrontendApp
 */
angular.module('Eventure')
	.controller('SignupController', ['$scope', 'Authentication', '$http', '$uibModalInstance','$uibModal','$timeout','$document', function ($scope, Authentication, $http, $uibModalInstance, $uibModal, $timeout, $document) {
		$scope.newUser = {};


		$scope.signup = function () {
			// call Authentication.register function
			Authentication.signup($scope.newUser, $uibModalInstance);
			$scope.UsernameError = null;
		};

		$scope.checkUsername = function () {
      console.log($scope.newUser.username);
			//only do a api call if username field is atleast min value of field
			if($scope.newUser.username.length >= 4) {
				$http.get("http://localhost:9000/api/v1/user/checkUsername?username=" + $scope.newUser.username)
					.success(function (bool) {

						if(bool)
							$scope.UsernameError = "Username Taken.";
						else
							$scope.UsernameError = null;
					})
			}
		}

		$scope.toLogin = function () {

			$uibModalInstance.close();

			$timeout(function () {

				var parentElem = angular.element($document[0].querySelector('#loginmodal-wrapper'));

				var modalInstance = $uibModal.open({
					templateUrl: '../../views/partials/login-modal.html',
					controller: 'LoginController',
					appendTo: parentElem
				});

				modalInstance.result.then(function () {}, function () {});
			});


		}

  }]);
