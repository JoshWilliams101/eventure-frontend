'use strict';


var app = angular.module("Eventure");



app.controller('MainController',['$scope', 'Restangular', '$http', 'Socket','$rootScope','$timeout', '$document','$location', 'Notification', function ($scope, Restangular, $http, SocketFactory, $rootScope, $timeout, $document, $location, Notification) {

  $scope.category = function() {
    $location.path('categoryEvents/' + $scope.selected);
  };

  $scope.datePicker = {};
  $scope.datePicker.date = {startDate: null, endDate: null};
  $scope.datePickerOptions = {
    locale: {
      fromLabel: "FROM",
      toLabel: "TO",
      format: 'MMM Do, YYYY'
    },
    ranges: {
            'Prev Month': [moment().subtract(1, 'months'), moment()],
            'Prev Week': [moment().subtract(7, 'days'), moment()],
            'Yesterday': [moment().subtract(2, 'days'), moment().add(1, 'days')],
            'Today': [moment(), moment().add(1, 'days')],
            'Tommorow': [moment().add(1, 'days'), moment().add(2, 'days')],
            'Next Week': [moment(),moment().add(7, 'days')],
            'Next Month': [moment(),moment().add(1, 'months')]
        }
  };

  $scope.$watch('datePicker.date', function(newDate) {
      if($rootScope.currentUser) {
        $scope.getFilteredEvents(newDate);
      }
      else {
        $scope.getAllEvents(newDate);
      }
    },
  false);

  //options array for category filter select
  $scope.categories = [{
    value: "8",
    text: "Art"
  }, {
    value: "9",
    text: "Business"
  }, {
    value: "15",
    text: "Casual"
  }, {
    value: "6",
    text: "Community"
  }, {
    value: "16",
    text: "Convention"
  }, {
    value: "5",
    text: "Eduction"
  }, {
    value: "12",
    text: "Fashion"
  }, {
    value: "11",
    text: "Film & Media"
  }, {
    value: "4",
    text: "Fitness"
  }, {
    value: "17",
    text: "Food & Drink"
  }, {
    value: "2",
    text: "Gaming"
  }, {
    value: "14",
    text: "Hobbies"
  }, {
    value: "10",
    text: "Music"
  }, {
    value: "1",
    text: "Outdoors"
  }, {
    value: "13",
    text: "Spirituality"
  }, {
    value: "3",
    text: "Sports"
  }, {
    value: "7",
    text: "Technology"
  }];

  //main page carousel data
  $scope.carousel = [{
    src: "../../images/events_1.jpg",
    title: "Event 1",
    active: true
  }, {
    src: "../../images/adventure_1.jpg",
    title: "Adventure 1",
    active: false
  }, {
    src: "../../images/events_2.jpg",
    title: "Adventure 1",
    active: false
  }, {
    src: "../../images/adventure_2.jpg",
    title: "Adventure 1",
    active: false
  }, {
    src: "../../images/adventure_3.jpg",
    title: "Adventure 1",
    active: false
  }, ];


  //events object
	$scope.events = [];
  $scope.newEventCounter = 0;

  //api base url for events
  var baseEvents = Restangular.all('events');


 $scope.getAllEvents = function(params){



  //get all events
  baseEvents.customGET("", {
    start_date: (params && params.startDate) ? params.startDate._d : undefined,
    end_date: (params && params.endDate) ? params.endDate._d : undefined
  }).then(function(events) {

    $scope.events = [];

    //loop through each event
    for (var i in events.data) {

      //check if user is the owner of the event
      if ($rootScope.currentUser && $rootScope.currentUser.id === events.data[i].user_id) {
        events.data[i].isOwner = true;
      }

      //loop through users in events
      for (var x in events.data[i].users) {

        //if the user is logged in and has joined this event set joined value to true
        if($rootScope.currentUser && $rootScope.currentUser.id === events.data[i].users[x].user_id) {
          events.data[i].joined = true;
          break;
        }
      }
      events.data[i].start_date = new Date(events.data[i].start_date);
      events.data[i].end_date = new Date(events.data[i].end_date);

      $scope.events.push(events.data[i]);
    }
  }).catch(function (err) {
    Notification.error({message: err.message, positionX: 'left'});
  });
 };


 var baseInterestsEvents = Restangular.all('interests/categories/events');

 $scope.getFilteredEvents = function (params) {
   baseInterestsEvents.customGET("", {
     start_date: (params && params.startDate) ? params.startDate._d : undefined,
     end_date: (params && params.endDate) ? params.endDate._d : undefined
   }).then(function (events) {


     $scope.events = [];

     //loop through each event
     for (var i in events.data) {

       //check if user is the owner of the event
       if ($rootScope.currentUser && $rootScope.currentUser.id === events.data[i].user_id) {
         events.data[i].isOwner = true;
       }

       //loop through users in events
       for (var x in events.data[i].users) {

         //if the user is logged in and has joined this event set joined value to true
         if($rootScope.currentUser && $rootScope.currentUser.id === events.data[i].users[x].user_id) {
           events.data[i].joined = true;
           break;
         }
       }
       events.data[i].start_date = new Date(events.data[i].start_date);
       events.data[i].end_date = new Date(events.data[i].end_date);

       $scope.events.push(events.data[i]);
     }

     if($scope.events.length <= 0) {
       Notification.warning({message: 'There are no events based on your interests, fetching all events, update your interests under profile settings <a href="/profile" ><strong class="text-primary">HERE</strong></a>', positionX: 'left'});
       $scope.getAllEvents($scope.datePicker.date);
     }
   }).catch(function (err) {
     Notification.error({message: err.message, positionX: 'left'});
   });
 };




  //for pagination on main page
  $scope.pageSize = 12;
  $scope.currentPage = 1;


  ////////////////////////////
  ///// SOCKET CODE BELOW
  ////////////////////////////


  //store main socket from Factory
  var socket = SocketFactory.mainSocket().socket;


  $scope.showButton = false;
  $scope.loadMoreEvents = function(){
    $scope.newEventCounter = 0;
    $scope.showButton = false;
    $scope.events = [];
    $scope.getAllEvents();
  }

  //when someone creates an event, update page in realtime
  socket.on('eventCreated', function (event) {
    console.log("event from socket is: ", event);
    $timeout(function(){
      $scope.newEventCounter++;
      if($scope.newEventCounter % 3 == 0){
        $scope.showButton = true;
      }
    });
  });
}]);
