'use strict';

/**
 * @ngdoc function
 * @name Eventure.controller:TestCtrl
 * @description
 * # TestCtrl
 * Controller of the Eventure
 */
var app = angular.module('Eventure');

app.controller('TestController',['$scope', 'Restangular', function ($scope, Restangular) {

  //base api url for events
	var baseEvents = Restangular.all('events');

  //get all events
	baseEvents.customGET("", {}).then(function (events) {
		$scope.events = events.data;
		console.log($scope.events);
	});

}]);
