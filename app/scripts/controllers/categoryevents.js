'use strict';

/**
 * @ngdoc function
 * @name Eventure.controller:CategoryeventsCtrl
 * @description
 * # CategoryeventsCtrl
 * Controller of the Eventure
 */
angular.module('Eventure')
  .controller('CategoryEventsController', ['$routeParams', '$scope', '$rootScope', 'Restangular', 'NgMap', '$timeout', '$location', function($routeParams, $scope, $rootScope, Restangular, NgMap, $timeout, $location) {
    $scope.latitude = $rootScope.lat || 43.6574088;
    $scope.longitude = $rootScope.lng || -79.7437504;

    //ng style for category title
    $scope.categorytitle = {
      "border-top-right-radius": "5px",
      "border-top-left-radius": "5px",
      "color": "white",
      "padding": "10px",
    };

    $scope.toCreateEvent = function () {
      $timeout(function () {
        $location.path('/createEvent');

      })
    };

    //datepicker setup
    $scope.datePicker = {};
    $scope.datePicker.date = {startDate: null, endDate: null};
    $scope.datePickerOptions = {
      locale: {
        fromLabel: "FROM",
        toLabel: "TO",
        format: 'MMM Do, YYYY'
      },
      ranges: {
            'Prev Month': [moment().subtract(1, 'months'), moment()],
            'Prev Week': [moment().subtract(7, 'days'), moment()],
            'Yesterday': [moment().subtract(2, 'days'), moment().add(1, 'days')],
            'Today': [moment(), moment().add(1, 'days')],
            'Tommorow': [moment().add(1, 'days'), moment().add(2, 'days')],
            'Next Week': [moment(),moment().add(7, 'days')],
            'Next Month': [moment(),moment().add(1, 'months')]
          }
    };

    $scope.$watch('datePicker.date', function(newDate) {
          $scope.getCategoryEvents(newDate);
      },
    false);

    //instantiate object for category specific events
    $scope.categoryevents = [];

    //instantiate object for category
    $scope.category = {};

    //array of markers
    $scope.eventMarkers = [];

    //on marker click
    $scope.toggleBounce = function() {
      if (this.getAnimation() != null) {
        this.setAnimation(null);
      } else {
        this.setAnimation(google.maps.Animation.BOUNCE);
      }
    }

    //base Events for specified event by ID
    var baseCategory = Restangular.one('categories/', $routeParams.categoryId);


    $scope.getCategoryEvents = function (params) {

      console.log(params);


      //base category events for specified event by categoryID
      var baseCategoryEvents = Restangular.all('categories/' + $routeParams.categoryId + "/events");
      //get all events that are linked to this category
      baseCategoryEvents.customGET("", {
        start_date: (params && params.startDate) ? params.startDate._d : undefined,
        end_date: (params && params.endDate) ? params.endDate._d : undefined
      }).then(function(event) {
        $scope.category = event.data;

        $scope.eventMarkers = [];
        $scope.categoryevents = [];


        angular.forEach(event.data.events, function($event) {
            $scope.categoryevents.push($event);

            //if true push marker
            if ($event.latitude && $event.longitude) {
              $scope.eventMarkers.push([$event.latitude, $event.longitude]);
            }
        });

        //fit bounds to markers on the map
        var bounds = new google.maps.LatLngBounds();
        angular.forEach($scope.eventMarkers, function($location) {
          var latlng = new google.maps.LatLng($location[0], $location[1]);
          bounds.extend(latlng);
        });

        //set map bounds using markers
        NgMap.getMap().then(function(map) {
          map.setCenter(bounds.getCenter());
          map.fitBounds(bounds);
        });
      });
    };

    //call function on load
    $scope.getCategoryEvents();
  }]);
