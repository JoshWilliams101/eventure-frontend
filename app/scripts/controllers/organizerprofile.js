'use strict';

/**
 * @ngdoc function
 * @name Eventure.controller:OrganizerprofileCtrl
 * @description
 * # OrganizerprofileCtrl
 * Controller of the Eventure
 */
angular.module('Eventure')
  .controller('OrganizerProfileController', ['$scope', 'Restangular', '$routeParams', function($scope, Restangular, $routeParams) {

    //initialize past events array
    $scope.passedEvents = [];

    //initialize current events array
    $scope.currentEvents = [];

    //base user for specified user by ID
    var baseUser = Restangular.one('/users/' + $routeParams.user_id);
    //get user by user id
    baseUser.customGET("", {}).then(function(event) {
      $scope.user = event.data;
      console.log($scope.user.username);

      if($scope.user.first_name == null){
        $scope.noname = true;
        $scope.nobirthdate = true;
      }

    });

    //base user events for specified userID
    var baseUserEvents = Restangular.all('/users/' + $routeParams.user_id + '/createdEvents');
    //get user by user id
    baseUserEvents.customGET("", {}).then(function(event) {
      $scope.userEvents = event.data;

      for(var i in event.data) {
        var eventdate = new Date(event.data[i].end_date);
        console.log(eventdate);
        if (eventdate < Date.now()) {
          $scope.passedEvents.push(event.data[i]);
        } else {
          $scope.currentEvents.push(event.data[i]);
        }
      }
    });


  }]);
