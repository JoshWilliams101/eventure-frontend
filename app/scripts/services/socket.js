(function(){

'use strict';

/**
 * @ngdoc service
 * @name eventureFrontendApp.socket
 * @description
 * # socket
 * Factory in the eventureFrontendApp.
 */


angular.module('Eventure')
	.factory('Socket', function () {


		return {

			mainSocket: function () {
				this.socket = io.connect('http://localhost:9000', {'forceNew': true});
				var self = this;
				this.on = function (eventName, callback) {
					self.socket.on(eventName, callback);
				};
				this.emit = function (eventName, data) {
					self.socket.emit(eventName, data);
				};
				return this;
			},

			messageSocket: function () {
				this.socket = io.connect('http://localhost:9000/messages', {'forceNew': true});
				var self = this;
				this.on = function (eventName, callback) {
					self.socket.on(eventName, callback);
				};
				this.emit = function (eventName, data) {
					self.socket.emit(eventName, data);
				};
				return this;
			},


		};
	});
})()
