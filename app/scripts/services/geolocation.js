'use strict';

/**
 * @ngdoc service
 * @name Eventure.geolocation
 * @description
 * # geolocation
 * Factory in the Eventure.
 */
angular.module('Eventure')
  .factory('geolocation',['$rootScope','$http', function ($rootScope, $http) {

    return {

      //grabbing location by ip
      getLocationByIp : function () {
        $http.get("http://ipinfo.io/", {withCredentials: false})
          .then(function (response) {
            //variable to store City based on IP
            $rootScope.geoCity = response.data.city + ", " + response.data.region;
            var location = response.data.loc.split(",");

            //store $rootScope.latitude and $rootScope.longitude
            $rootScope.loc = {
              lat: location[0],
              lng: location[1],
            };
          })
          .catch(function (response) {
            $rootScope.geoCity = null;
            $rootScope.loc = null;
            console.error(response.status + " " + response.data);
          });
      },
    };
  }]);
