'use strict';

/**
 * @ngdoc service
 * @name eventureFrontendApp.Authentication
 * @description
 * # Authentication
 * Factory in the eventureFrontendApp.
 */
angular.module('Eventure')
	.factory('Authentication', ['$rootScope', '$http','$route', '$timeout', function ($rootScope, $http, $route, $timeout) {


		var authObject = {

			login: function (user, modalInstance) {

				$http.post("http://localhost:9000/local_login", user)
					.success(function (data) {
						$rootScope.LoginError = null;
						$rootScope.SignupError = null;
						$rootScope.currentUser = {
              loginType: data.loginType,
							id: data.user.id,
							profile_pic_url: data.user.profile_pic_url,
							date_of_birth: data.user.date_of_birth,
							gender: data.user.gender,
							first_name: data.user.first_name,
							last_name: data.user.last_name,
							displayName: chooseDisplayName(data)
						};

						$timeout(function () {
							//re render page their on
							$route.reload();
						});


						//close modal
						modalInstance.close();


					})
					.catch(function (err) {
						if(err) {
							$rootScope.LoginError = err.data.message;
						}
					});
			},

			signup: function (user, modalInstance) {
				$http.post("http://localhost:9000/local_signup", user)
					.success(function (data) {

						$rootScope.LoginError = null;
						$rootScope.SignupError = null;
						$rootScope.isLoggedIn = true;
						$rootScope.currentUser = {
              loginType: data.user.loginType,
							id: data.user.id,
							profile_pic_url: data.user.profile_pic_url,
							date_of_birth: data.user.date_of_birth,
							gender: data.user.gender,
							first_name: data.user.first_name,
							last_name: data.user.last_name,
							displayName: chooseDisplayName(data)
						};

						$timeout(function () {
							//re render page their on
							$route.reload();
						});
						//close modal
						modalInstance.close();

					})
					.catch(function (err) {
						if(err) {
							$rootScope.SignupError = err.data.message;
						}
					});
			},

			isLoggedIn: function () {
				$http.get("http://localhost:9000/loggedin")
					.success(function (data) {

						//if user is logged in on server but not on app
						if($rootScope.currentUser == null && data.success) {
							$rootScope.currentUser = {
                loginType: data.user.loginType,
								id: data.user.id,
								date_of_birth: data.user.date_of_birth,
								gender: data.user.gender,
								first_name: data.user.first_name,
								last_name: data.user.last_name,
								profile_pic_url: data.user.profile_pic_url,
								displayName: chooseDisplayName(data)
							};

						} else {
							//set currentUser to null
							$rootScope.currentUser = null
						}
					})
					.catch(function (err) {
						//set currentUser to null
						$rootScope.currentUser = null;
					});
			},

		};

    function chooseDisplayName(userObj) {

      userObj = userObj.user;

      if(userObj.loginType ===  'local') {
        return userObj.username;
      }
      else if(userObj.loginType ===  'facebook') {
        return userObj.facebook_name;
      }
      else if(userObj.loginType ===  'twitter') {
        return userObj.twitter_displayName;
      }
      else {
        return userObj.google_name;
      }

    }

		return authObject;
  }]);
