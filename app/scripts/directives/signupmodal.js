'use strict';

/**
 * @ngdoc directive
 * @name Eventure.directive:signupModal
 * @description
 * # signupModal
 */
var app = angular.module('Eventure');
app.directive('signupModal', function () {
	return {
		templateUrl: '../views/partials/signup-modal.html',
		restrict: 'E',
		replace: true,
	};
});
