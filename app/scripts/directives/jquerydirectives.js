'use strict';

/**
 * @ngdoc directive
 * @name Eventure.directive:jquerydirectives
 * @description directive file for all jquery related functionality
 * #
 */
var app = angular.module('Eventure');


//toggle class directive
app.directive('toggleClassLeave', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('click', function() {
                element.toggleClass(attrs.toggleClass);

                if(element.hasClass(attrs.toggleClass)) {
                  element.text('Leave Event');
                }
                else {
                  element.text('Join Event');
                }
            });
        }
    };
});

//directive that restricts a user to log in
app.directive('userRequired',['$rootScope','$uibModal','$document', function ($rootScope, $uibModal, $document) {
  return {
    restrict: 'A',
    scope: {
      callback: '=userRequiredCallback'
    },
    link: function (scope, element, attrs) {
      element.bind('click', function () {
        if(!$rootScope.currentUser) {
          $rootScope.LoginError = "Please log in";
          var parentElem = angular.element($document[0].querySelector('#loginmodal-wrapper'));

      		var modalInstance = $uibModal.open({
      			templateUrl: '../../views/partials/login-modal.html',
      			controller: 'LoginController',
      			appendTo: parentElem
      		});

      		modalInstance.result.then(function () {}, function () {});
        }
        else {
          //if scope.callback is defined excute function
          if(scope.callback) {
            scope.callback();
          }
        }
      });
    }
  }
}]);

//directive for enter pressed
app.directive('enterPressed', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });
                event.preventDefault();
            }
        });
    };
});

app.directive('notificationsDropDown', function () {
  return {
    restrict: 'E',
    scope: {
      events : ' =e',
      showNotifications: ' =shown' //actually notifications/n
    },
    templateUrl: '../../views/partials/notifications.html',
    link: function(scope){
      console.log(scope);
    },
  };
});
