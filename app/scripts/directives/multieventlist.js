'use strict';

/**
 * @ngdoc directive
 * @name Eventure.directive:multiEvent
 * @description
 * # multiEvent
 */
var app = angular.module('Eventure')
app.directive('multiEventList', ['$rootScope', function ($rootScope) {
	return {
		templateUrl: '../views/partials/event-list-multi-col.html',
		restrict: 'E',
		replace: true,
		scope: {
			events     : ' =e',
			pageSize   : ' =ps',
			currentPage: ' =cp',
			query      : ' =q',
		},
		link: function (scope) {
			scope.today = new Date();
		}
	};
}]);
