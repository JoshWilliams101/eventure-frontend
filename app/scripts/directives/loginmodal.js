'use strict';

/**
 * @ngdoc directive
 * @name Eventure.directive:loginModal
 * @description
 * # loginModal
 */
var app = angular.module('Eventure');
app.directive('loginModal', function () {
	return {
		templateUrl: '../views/partials/login-modal.html',
		restrict: 'E',
		replace: true,
	};
});
