'use strict';

/**
 * @ngdoc directive
 * @name Eventure.directive:eventureNavbar
 * @description
 * # eventureNavbar
 */
var app = angular.module('Eventure');
app.directive('eventureNavbar', function () {
	return {
		templateUrl: '../views/partials/navbar.html',
		restrict: 'E',
		replace: true,
	};
});
