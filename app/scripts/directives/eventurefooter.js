'use strict';

/**
 * @ngdoc directive
 * @name Eventure.directive:eventureFooter
 * @description
 * # eventureFooter
 */
var app = angular.module('Eventure');
app.directive('eventureFooter', function () {
	return {
		restrict: 'E',
		templateUrl: '../../views/partials/footer.html',
		replace: true,
	};
});
