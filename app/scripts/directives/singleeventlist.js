'use strict';

/**
 * @ngdoc directive
 * @name Eventure.directive:singleEventList
 * @description
 * # singleEventList
 */
var app = angular.module('Eventure')
app.directive('singleEventList', function () {
	return {
		templateUrl: '../views/partials/event-list-single-col.html',
		restrict: 'E',
		replace: true,
		scope: {
			events     : ' =e',
			pageSize   : ' =ps',
			currentPage: ' =cp',
			query      : ' =q'
		}
	};
});
