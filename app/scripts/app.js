'use strict';

/**
 * @ngdoc overview
 * @name eventureFrontendApp
 * @description
 * # eventureFrontendApp
 *
 * Main module of the application.
 */
var app = angular.module('Eventure', [
   'ngAnimate',
   'ngCookies',
   'ngResource',
   'ngTouch',
   'ngSanitize',
   'ngRoute',
   'restangular',
   'ngMap',
   'ui.bootstrap',
   'google.places',
   'ui.tinymce',
   'checklist-model',
   'ngFileUpload',
   'angularUtils.directives.dirDisqus',
   'angular-notification-icons',
   'daterangepicker',
   'mwl.calendar',
   'ui-notification'
 ]);

 app.filter('startFrom', function(){
   return function(data, start){
     data = data || [];
     return data.slice(start);
   };
 });

 app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
   $routeProvider.
     when('/main', {
       templateUrl: 'views/main.html',
       controller: 'MainController'
     }).
     when('/eventDetails/:eventId', {
       templateUrl: 'views/eventdetails.html',
       controller: 'EventDetailsController'
     }).
     when('/test', {
       templateUrl: 'test.html',
       controller: 'TestController'
     }).
     when('/createEvent', {
       templateUrl: 'views/createEvent.html',
       controller: 'CreateEventController'
     }).
     when('/profile', {
       templateUrl: 'views/profile.html',
       controller: 'ProfileController'
     }).
     when('/notifications', {
       templateUrl: 'views/notifications.html',
       controller: 'NotificationsController'
     }).
     when('/categoryEvents/:categoryId', {
       templateUrl: 'views/categoryevents.html',
       controller: 'CategoryEventsController'
     }).
     when('/editevent/:eventId', {
       templateUrl: 'views/editevent.html',
       controller: 'EditEventController'
     }).
     when('/organizerProfile/:user_id', {
       templateUrl: 'views/organizerProfile.html',
       controller: 'OrganizerProfileController'
     }).
     otherwise({
       redirectTo:'main'
     });

     $locationProvider.html5Mode({
       enabled: true,
       requireBase: true
     });
   }])
	.config(function (RestangularProvider) {
		RestangularProvider.setBaseUrl('http://localhost:9000/api/v1');
	})
	.config(function ($httpProvider) {
	  //set default ajax requests to contain withCredentials = true
	  $httpProvider.defaults.withCredentials = true;
	})
	.run(['$rootScope', '$http', 'Authentication','geolocation','$timeout','$route', '$uibModalStack','$uibModal', '$document', '$location', 'Socket', function ($rootScope, $http, Authentication, geolocation, $timeout, $route, $uibModalStack, $uibModal, $document, $location, SocketFactory) {

    //close modal when route changed
    $uibModalStack.dismissAll();

		//check if user is logged in through social media, or in general on backend
		Authentication.isLoggedIn();


    $rootScope.logout = function () {
      $http.get("http://localhost:9000/logout")
        .success(function () {
          $timeout(function () {
            $rootScope.currentUser = null;
            //re-render the route your on unless its route requires permissions
            if($route.current.loadedTemplateUrl.includes("createEvent") || $route.current.loadedTemplateUrl.includes("notifications") || $route.current.loadedTemplateUrl.includes("profile")) {

              $location.path('main');

            } else {
              $route.reload();

            }

          });
        }). error(function () {
          console.log("error");
        });
    };

    //get location by ip and store results in $rootScope variable
    geolocation.getLocationByIp();

    $rootScope.$on("$locationChangeStart", function(event, next, current) {

      //if someone tries to access an authenticated route directly
      if(!$rootScope.currentUser && (next.includes("createEvent") || next.includes("profile") || next.includes("notifications")) && (current.includes("createEvent") || current.includes("profile") || current.includes("notifications"))) {
        event.preventDefault();
        $location.path('main');
        openLoginModal();
        return;
      }

        //if user is trying to reach a route that requires them to be logged in and they're not reject it.
        var reject = (!$rootScope.currentUser && (next.includes("createEvent") || next.includes("profile") || next.includes("notifications")));

        if(reject) {
          openLoginModal();
          event.preventDefault();
        }

        //all is well carry on
        return $uibModalStack.dismissAll();
    });


    function openLoginModal() {
      var parentElem = angular.element($document[0].querySelector('#loginmodal-wrapper'));

      var modalInstance = $uibModal.open({
        templateUrl: '../../views/partials/login-modal.html',
        controller: 'LoginController',
        appendTo: parentElem
      });

      modalInstance.result.then(function () {}, function () {});
    };


    //setup one main socket to use throughout the app
    $rootScope.mainSocket = SocketFactory.mainSocket().socket;



	}]);
